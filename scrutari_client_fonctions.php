<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
* $markedStringArray est un tableau qui contient soit des chaines soit des tableaux associatifs
* comprenant une clé 's' indiquant les chaines marquées. Ces dernières sont entourées d'une balise
* span dont la classe est $spanClass
*/
function concatWithSpan($markedStringArray) {
    if (!is_array($markedStringArray)) {
        return '';
    }
	$result = "";
	foreach ($markedStringArray as $obj) {
		if (is_object($obj)) {
			$result .= '<span class="spip_surligne">';
			$result .= $obj->s;
			$result .= '</span>';
		} elseif (is_array($obj)) {
			$result .= '<span class="spip_surligne">';
			$result .= $obj['s'];
			$result .= '</span>';
		} else {
			$result .= $obj;
		}
	}
	return $result;
}

function initMotcleStringMap($motcleArray) {
	$motcleStringMap = array();
	$count = count($motcleArray);
	for($i = 0; $i < $count; $i++) {
		$motcle = $motcleArray[$i];
		$codemotcle = $motcle['codemotcle'];
		$mlibelleArray = $motcle['mlibelleArray'];
		$lib = "";
		$libCount = count($mlibelleArray);
		for($j = 0; $j < $libCount; $j++) {
			if ($j > 0) {
				$lib .= "/";
			}
			$mlib =$mlibelleArray[$j];
			$lib .= concatWithSpan($mlib['mlib']);
		}
		$motcleStringMap[$codemotcle] = $lib;
	}
	return $motcleStringMap;
}

function initCorpusIntituleMap($corpusIntituleArray) {
	$corpusIntituleMap = array();
	$count = count($corpusIntituleArray);
	for ($i=0; $i < $count; $i++) {
		$corpus = $corpusIntituleArray[$i];
		foreach ($corpus as $cle => $valeur) {
			if ($cle == 'codecorpus') {
				$codecorpus = $valeur;
			}
			else {
				$corpusIntituleMap[$codecorpus][$cle] = $valeur;
			}
		}
	}
	return $corpusIntituleMap;
}

function initAttributIntituleMap($attributIntituleArray) {
	$attributIntituleMap = array();
	$count = count($attributIntituleArray);
	for ($i=0; $i < $count; $i++) {
		$attribut = $attributIntituleArray[$i];
		$name = $attribut['name'];
		$title = $attribut['title'];
		$attributIntituleMap[$name] = $title;
	}
	return $attributIntituleMap;
}

function filtre_scrutari_echappement($texte) {
    $texte  = preg_replace('/[^-*":!()&|[:alnum:]]+/u', ' ', $texte);
    $texte = trim(str_replace("\"", "\\\"", $texte));
    return $texte;
}
