<?php

if (!defined("_ECRIRE_INC_VERSION")) {
    return;
}

function scrutari_client_upgrade($nom_meta_base_version, $version_cible) {
    include_spip('base/upgrade');
    $maj = array();
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function scrutari_client_vider_tables($nom_meta_base_version) {
	include_spip('inc/config');
	effacer_config('scrutari_client');
    include_spip('inc/meta');
    effacer_meta($nom_meta_base_version);
}
