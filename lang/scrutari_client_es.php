<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	'choix_moteur' => "Choix du moteur",
	'configurer_scrutari_client' => "Configurer le Client Scrutari",
	'connexion_echoue' => "La connexion a échoué",

	'groupe_moteurs_url' => 'URL du groupe',

	'link_atom' => "Flujo de sindicación ATOM",
	'link_ods' => "Planilla en el formato ODS",
	'links' => "Recuperación de la integralidad de las referencias",

	'scrutari-motscles_plusieurs' => "Palabras-claves",
	'scrutari-motscles_un' => "Palabra-clave",

	'title_main' => "Búsqueda Scrutari",
	'titre_scrutari_client' => "Client Scrutari",

	'url_invalide' => "URL invalide"
];
