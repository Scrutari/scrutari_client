<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	'choix_moteur' => "Choix du moteur",
	'configurer_scrutari_client' => "Configurer le Client Scrutari",
	'connexion_echoue' => "La connexion a échoué",

	'groupe_moteurs_url' => 'URL du groupe',

	'link_atom' => "ATOM feed",
	'link_ods' => "Table in ODS format",
	'links' => "Retrieving all references:",

	'scrutari-motscles_plusieurs' => "Keywords:",
	'scrutari-motscles_un' => "Keyword:",

	'title_main' => "Scrutari search",
	'titre_scrutari_client' => "Client Scrutari",

	'url_invalide' => "URL invalide"
];
