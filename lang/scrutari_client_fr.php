<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	'choix_framework_css' => "Choix d'un framework CSS",
	'choix_moteur' => "Choix du moteur",
	'configurer_scrutari_client' => "Configurer le Client Scrutari",
	'connexion_echoue' => "La connexion a échoué",

	'groupe_moteurs_url' => 'URL du groupe',

	'link_atom' => "Flux de syndication ATOM",
	'link_ods' => "Tableau au format ODS",
	'links' => "Récupération de l’intégralité des références&nbsp;:",

	'recherche_scrutari' => "Recherche Scrutari",

	'scrutari-motscles_plusieurs' => "Mots-clés",
	'scrutari-motscles_un' => "Mot-clé",

	'title_main' => "Recherche Scrutari",
	'titre_scrutari_client' => "Client Scrutari",

	'url_invalide' => "URL invalide"
];
