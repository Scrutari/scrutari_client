<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	'choix_moteur' => "Choix du moteur",
	'configurer_scrutari_client' => "Configurer le Client Scrutari",
	'connexion_echoue' => "La connexion a échoué",

	'groupe_moteurs_url' => 'URL du groupe',

	'link_atom' => "flusso ATOM",
	'link_ods' => "cartella formato ODS",
	'links' => "recupero dell’integralità dei risultati",

	'scrutari-motscles_plusieurs' => "parole chiave:",
	'scrutari-motscles_un' => "parola chiave:",

	'title_main' => "ricerca scrutari",
	'titre_scrutari_client' => "Client Scrutari",

	'url_invalide' => "URL invalide"
];
