<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/config');

function formulaires_configurer_scrutari_client_charger_dist(){
	$url_groupe_moteurs = lire_config('scrutari_client/url_groupe_moteurs');
	$url_moteur = lire_config('scrutari_client/url_moteur');

	$type_url = '';
	if ($url_groupe_moteurs) {
		$type_url = 'groupe';
	}
	if ($url_moteur) {
		$type_url = 'moteur';
	}

	$liste_frameworks = array();
	$d = opendir(find_in_path('lib/scrutarijs/frameworks'));
	if ($d) {
		while (($f = readdir($d)) !== false) {
			if (preg_match(",^(\w+)\.js$,", $f, $matches)) {
				$liste_frameworks[] = $matches[1];
			}
		}
		closedir($d);
	}
	sort($liste_frameworks);

	$valeurs = array(
		'type_url' => $type_url,
		'url_groupe_moteurs' => $url_groupe_moteurs,
		'url_moteur' => $url_moteur,
		'moteurs' => lire_config('scrutari_client/moteurs'),
		'framework_css' => lire_config('scrutari_client/framework_css', 'none'),
		'liste_frameworks' => $liste_frameworks,
		'valider_groupe' => '',
		'valider_moteurs' => ''
	);
	return $valeurs;
}

function verifier_url($url) {
	if (!$url) {
		return _T('info_obligatoire');
	}
	if (!preg_match('/http[s]?\:\/\/[a-z0-9+\$_-]+(?:\.[a-z0-9+\$_-]+)+(?:\:[0-9]{2,5})?(?:\/(?:[a-z0-9+\$_%,-]\.?)+)*\/?(?:\?[a-z+&$_.-][a-z0-9;:@\/&%=+$_.-]*)?(?:#[a-z_.-][a-z0-9+\$_.-]*)?/i', $url)) {
		return  _T('scrutari_client:url_invalide');
	}
	include_spip('inc/distant');
	if (recuperer_url($url, ['methode' => 'HEAD', 'taille_max' => 0]) === false) {
		return _T('scrutari_client:connexion_echoue');
	}

	return '';
}

function formulaires_configurer_scrutari_client_verifier_dist(){

	$erreurs = array();

	if (_request('valider_url')) {
		$type_url = _request('type_url');
		if (!$type_url) {
			$erreurs['type_url'] = _T('info_obligatoire');
			return $erreurs;
		}
		if ($type_url == 'groupe' && $erreur_url = verifier_url(_request('url_groupe_moteurs'))) {
			$erreurs['url_groupe_moteurs'] = $erreur_url;
			return $erreurs;
		}
		if ($type_url == 'moteur' && $erreur_url = verifier_url(_request('url_moteur'))) {
			$erreurs['url_moteur'] = $erreur_url;
			return $erreurs;
		}
	}

	if (_request('valider_moteurs')) {
		$compte_moteur = 0;
		$compte_defaut = 0;
		$moteurs = _request('moteurs');
		foreach ($moteurs as $nom_moteur=>$proprietes) {
			if (isset($proprietes['url']) && $proprietes['url']) {
				$compte_moteur++;
				if (isset($proprietes['defaut']) && $proprietes['defaut']) {
					$compte_defaut++;
				}
			}
		}
		if ($compte_moteur && $compte_defaut != 1) {
			$erreurs['defaut'] = "Vous devez sélectionner un moteur par défaut";
		}
	}

	return $erreurs;
}

function formulaires_configurer_scrutari_client_traiter_dist(){

	if (_request('valider_url')) {
		$type_url = _request('type_url');
		$url_groupe_moteurs = _request('url_groupe_moteurs');
		$url_moteur = _request('url_moteur');
		if ($type_url == 'groupe' && $url_groupe_moteurs != lire_config('scrutari_client/url_groupe_moteurs')) {
			effacer_config('scrutari_client');
			ecrire_config('scrutari_client/url_groupe_moteurs', $url_groupe_moteurs);
			set_request('moteurs');
		}
		if ($type_url == 'moteur' && $url_moteur != lire_config('scrutari_client/url_moteur')) {
			effacer_config('scrutari_client');
			ecrire_config('scrutari_client/url_moteur', $url_moteur);
			set_request('moteurs');
		}
	}

	if (_request('valider_moteurs')) {
		$moteurs = _request('moteurs');

		foreach ($moteurs as $nom_moteur=>$proprietes) {
			if (!isset($proprietes['url'])) {
				unset($moteurs[$nom_moteur]);
			}
		}

		ecrire_config('scrutari_client/moteurs', $moteurs);
		ecrire_config('scrutari_client/framework_css', _request('framework_css'));
	}

	return array('message_ok'=>_T('config_info_enregistree'), 'editable'=>true);
}
