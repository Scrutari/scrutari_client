<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2016                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_recherche_scrutari_etendue_charger_dist($lien = '', $class=''){
	return
		array(
			'recherche_scrutari' => _request('recherche_scrutari'),
			'moteur' => _request('moteur'),
			'class' => $class,
			'_id_champ' => $class ? substr(md5($lien.$class),0,4) : 'recherche_scrutari'
		);
}

function formulaires_recherche_scrutari_etendue_traiter_dist($lien = '', $class=''){
	if (_request('moteur') == 'spip') {
		$retour = generer_url_public(
			'recherche',
			array(
				'recherche' => _request('recherche_scrutari')
			)
		);
	}
	elseif ($lien) {
		$retour = parametre_url($lien, 'recherche_scrutari', _request('recherche_scrutari'));
		$retour = parametre_url($retour, 'moteur', _request('moteur'));
	}
	else {
		$retour = generer_url_public(
			'recherche_scrutari',
			array(
				'recherche_scrutari' => _request('recherche_scrutari'),
				'moteur' => _request('moteur')
			)
		);
	}

	return array('redirect' => $retour);
}
